document.getElementById('input').addEventListener('keydown', function(e) {
    if (e.key === 'Enter') {
        const command = e.target.value;
        processCommand(command);
        e.target.value = '';
    }
});

function processCommand(command) {
    const terminal = document.getElementById('terminal');
    terminal.innerHTML += `<span class="prompt">${command}</span>`;
    const [cmd, ...args] = command.split(' ');

    switch (cmd) {
        case 'github':
        case 'gitlab':
            handleGitCommand(cmd, args);
            break;
        case 'help':
            showHelp();
            break;
        case 'echo':
            echo(args.join(' '));
            break;
        case 'echoConsole':
            echoConsole(args.join(' '));
            break;
        case 'about':
            about();
            break;
        default:
            terminal.innerHTML += `<span class="error">${command} is not recognized as a known command. Type help for information.</span>`;
    }
    terminal.scrollTop = terminal.scrollHeight;  // Scroll to bottom
}

function handleGitCommand(cmd, args) {
    const terminal = document.getElementById('terminal');
    const platform = cmd === 'github' ? 'https://github.com' : 'https://gitlab.com';
    const [subCmd, ...subArgs] = args;

    switch (subCmd) {
        case 'list':
            listFiles(platform, subArgs);
            break;
        case 'file':
            showFileContents(platform, subArgs);
            break;
        case 'issues':
            listIssues(platform, subArgs);
            break;
        case 'pull':
            listPullRequests(platform, subArgs);
            break;
        case 'fork':
            forkRepo(platform, subArgs);
            break;
        case 'createIssue':
            createIssue(platform, subArgs);
            break;
        case 'createPullRequest':
            createPullRequest(platform, subArgs);
            break;
        case 'watchers':
            listWatchers(platform, subArgs);
            break;
        case 'stars':
            listStargazers(platform, subArgs);
            break;
        default:
            terminal.innerHTML += `<span class="error">Unknown git command</span>`;
    }
}

function showHelp() {
    const terminal = document.getElementById('terminal');
    terminal.innerHTML += `
    <span class="command">Available Commands:</span>
    <ul>
        <li><span class="link" onclick="showHelp()">help</span> - Shows this help message</li>
        <li><span class="link" onclick="echo('Your message')">echo [msg]</span> - Echoes a message</li>
        <li><span class="link" onclick="echoConsole('Your message')">echoConsole [msg]</span> - Echoes a message in the console</li>
        <li><span class="link" onclick="handleGitCommand('github', ['list', 'username', 'repository'])">github list [username] [repository]</span> - Lists all files in that GitHub repository</li>
        <li><span class="link" onclick="handleGitCommand('github', ['file', 'username', 'repository', 'path'])">github file [username] [repository] [path]</span> - Shows the contents of the specified file from GitHub</li>
        <li><span class="link" onclick="handleGitCommand('gitlab', ['list', 'username', 'repository'])">gitlab list [username] [repository]</span> - Lists all files in that GitLab repository</li>
        <li><span class="link" onclick="handleGitCommand('gitlab', ['file', 'username', 'repository', 'path'])">gitlab file [username] [repository] [path]</span> - Shows the contents of the specified file from GitLab</li>
        <li><span class="link" onclick="handleGitCommand('github', ['issues', 'username', 'repository'])">github issues [username] [repository]</span> - Lists all issues in a GitHub repository</li>
        <li><span class="link" onclick="handleGitCommand('gitlab', ['issues', 'username', 'repository'])">gitlab issues [username] [repository]</span> - Lists all issues in a GitLab repository</li>
        <li><span class="link" onclick="handleGitCommand('github', ['pull', 'username', 'repository'])">github pull [username] [repository]</span> - Lists all pull requests in a GitHub repository</li>
        <li><span class="link" onclick="handleGitCommand('gitlab', ['pull', 'username', 'repository'])">gitlab pull [username] [repository]</span> - Lists all merge requests in a GitLab repository</li>
        <li><span class="link" onclick="handleGitCommand('github', ['fork', 'username', 'repository'])">github fork [username] [repository]</span> - Forks a GitHub repository</li>
        <li><span class="link" onclick="handleGitCommand('gitlab', ['fork', 'username', 'repository'])">gitlab fork [username] [repository]</span> - Forks a GitLab repository</li>
        <li><span class="link" onclick="handleGitCommand('github', ['createIssue', 'username', 'repository', 'title', 'body'])">github createIssue [username] [repository] [title] [body]</span> - Creates an issue in a GitHub repository</li>
        <li><span class="link" onclick="handleGitCommand('gitlab', ['createIssue', 'username', 'repository', 'title', 'body'])">gitlab createIssue [username] [repository] [title] [body]</span> - Creates an issue in a GitLab repository</li>
        <li><span class="link" onclick="handleGitCommand('github', ['createPullRequest', 'username', 'repository', 'head', 'base', 'title', 'body'])">github createPullRequest [username] [repository] [head] [base] [title] [body]</span> - Creates a pull request in a GitHub repository</li>
        <li><span class="link" onclick="handleGitCommand('gitlab', ['createPullRequest', 'username', 'repository', 'title', 'description', 'source_branch', 'target_branch'])">gitlab createPullRequest [username] [repository] [title] [description] [source_branch] [target_branch]</span> - Creates a merge request in a GitLab repository</li>
        <li><span class="link" onclick="handleGitCommand('github', ['stars', 'username', 'repository'])">github stars [username] [repository]</span> - Lists all stargazers of a GitHub repository</li>
        <li><span class="link" onclick="about()">about</span> - Displays information about this terminal</li>
    </ul>`;
}

function echo(message) {
    const terminal = document.getElementById('terminal');
    terminal.innerHTML += `<span class="command">${message}</span>`;
}

function echoConsole(message) {
    console.log(message);
}

function about() {
    const terminal = document.getElementById('terminal');
    terminal.innerHTML += `<span class="command">Welcome to the CodespaceCMD Terminal for Git management!</span>`;
}

function listFiles(platform, [username, repo]) {
    fetch(`${platform}/${username}/${repo}/contents`)
        .then(response => response.json())
        .then(data => {
            data.forEach(file => {
                echo(`<a href="${file.html_url}" target="_blank" class="link">${file.name}</a>`);
            });
        })
        .catch(error => echo(`Error: ${error.message}`));
}

function showFileContents(platform, [username, repo, path]) {
    fetch(`${platform}/${username}/${repo}/contents/${path}`)
        .then(response => response.json())
        .then(data => {
            const content = atob(data.content);
            echo(`<pre>${content}</pre>`);
        })
        .catch(error => echo(`Error: ${error.message}`));
}

function listIssues(platform, [username, repo]) {
    fetch(`${platform}/${username}/${repo}/issues`)
        .then(response => response.json())
        .then(data => {
            data.forEach(issue => {
                echo(`<a href="${issue.html_url}" target="_blank" class="link">#${issue.number} - ${issue.title}</a>`);
            });
        })
        .catch(error => echo(`Error: ${error.message}`));
}

function listPullRequests(platform, [username, repo]) {
    fetch(`${platform}/${username}/${repo}/pulls`)
        .then(response => response.json())
        .then(data => {
            data.forEach(pr => {
                echo(`<a href="${pr.html_url}" target="_blank" class="link">#${pr.number} - ${pr.title}</a>`);
            });
        })
        .catch(error => echo(`Error: ${error.message}`));
}

function forkRepo(platform, [username, repo]) {
    const url = platform === 'https://github.com' ? `${platform}/${username}/${repo}/fork` : `${platform}/${username}/${repo}/fork`;
    window.open(url, '_blank');
    echo(`Forking repository: <a href="${url}" target="_blank" class="link">${url}</a>`);
}

function createIssue(platform, [username, repo, title, ...body]) {
    const url = platform === 'https://github.com' ? `${platform}/${username}/${repo}/issues/new` : `${platform}/${username}/${repo}/-/issues/new`;
    window.open(url, '_blank');
    echo(`Creating issue: <a href="${url}" target="_blank" class="link">${url}</a>`);
}

function createPullRequest(platform, [username, repo, ...rest]) {
    const url = platform === 'https://github.com' ? `${platform}/${username}/${repo}/compare` : `${platform}/${username}/${repo}/-/merge_requests/new`;
    window.open(url, '_blank');
    echo(`Creating pull request: <a href="${url}" target="_blank" class="link">${url}</a>`);
}


function listStargazers(platform, [username, repo]) {
    fetch(`${platform}/${username}/${repo}/stargazers`)
        .then(response => response.json())
        .then(data => {
            data.forEach(star => {
                echo(`<a href="${star.html_url}" target="_blank" class="link">${star.login}</a>`);
            });
        })
        .catch(error => echo(`Error: ${error.message}`));
}
